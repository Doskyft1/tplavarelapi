<?php

namespace Tests\Feature;

use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductStoreTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testStore()
    {
        $faker = Factory::create();

        $response = $this->post(route('products.store'), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween(0, 4294967295),
            'price' => $faker->randomFloat(2, 0,999999.99)
        ]);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testStoreMissPriceArgument()
    {
        $faker = Factory::create();

        $response = $this->post(route('products.store'), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testStoreWrongPriceArgument()
    {
        $faker = Factory::create();

        $response = $this->post(route('products.store'), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween(),
            'price' => $faker->word
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testStoreWrongStockArgument()
    {
        $faker = Factory::create();

        $response = $this->post(route('products.store'), [
            'name' => $faker->name,
            'stock' => $faker->word,
            'price' => $faker->randomFloat()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testStoreNegativePriceArgument()
    {
        $faker = Factory::create();

        $response = $this->post(route('products.store'), [
            'name' => $faker->name,
            'stock' => $faker->randomNumber(),
            'price' => -$faker->randomFloat()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

}
