<?php

namespace Tests\Feature;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;


class ProductTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testIndex()
    {
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShow()
    {
        $response = $this->get(route('products.show', ['product' => 2]));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFailed()
    {
        // NotFound
        $response = $this->get(route('products.show', ['product' => 10000000]));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
