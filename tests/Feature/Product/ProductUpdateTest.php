<?php

namespace Tests\Feature;

use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductUpdateTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testUpdate()
    {
        $faker = Factory::create();

        $response = $this->put(route('products.update', ['product' => 2]), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween(0, 4294967295),
            'price' => $faker->randomFloat(2, 0,999999.99)
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateNotFound()
    {
        $faker = Factory::create();

        $response = $this->put(route('products.update', ['product' => 1000000000]), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween(),
            'price' => $faker->randomFloat()
        ]);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateMissPriceArgument()
    {
        $faker = Factory::create();

        $response = $this->put(route('products.update', ['product' => 2]), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testUpdateWrongPriceArgument()
    {
        $faker = Factory::create();

        $response = $this->put(route('products.update', ['product' => 2]), [
            'name' => $faker->name,
            'stock' => $faker->numberBetween(),
            'price' => $faker->word
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testUpdateWrongStockArgument()
    {
        $faker = Factory::create();

        $response = $this->put(route('products.update', ['product' => 2]), [
            'name' => $faker->name,
            'stock' => $faker->word,
            'price' => $faker->randomFloat()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testUpdateNegativeStockArgument()
    {
        $faker = Factory::create();

        $response = $this->put(route('products.update', ['product' => 2]), [
            'name' => $faker->name,
            'stock' => $faker->randomNumber(),
            'price' => -$faker->randomFloat()
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
