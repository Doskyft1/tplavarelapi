<?php

namespace Tests\Feature;

use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductDestroyTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testDestroy()
    {
        $response = $this->delete(route('products.destroy', ['product' => 2]));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDestroyFailed()
    {
        $response = $this->delete(route('products.destroy', ['product' => 1000000]));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
