<?php

namespace Tests\Feature;

use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoryStoreTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testStore()
    {
        $faker = Factory::create();

        $response = $this->post(route('categories.store'), [
            'name' => $faker->name
        ]);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testStoreMissNameArgument()
    {
        $response = $this->post(route('categories.store'), []);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
