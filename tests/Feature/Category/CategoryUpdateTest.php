<?php

namespace Tests\Feature;

use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoryUpdateTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testUpdate()
    {
        $faker = Factory::create();

        $response = $this->put(route('categories.update', ['category' => 2]), [
            'name' => $faker->name
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateNotFound()
    {
        $faker = Factory::create();

        $response = $this->put(route('categories.update', ['category' => 1000000000]), [
            'name' => $faker->name
        ]);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateMissNameArgument()
    {
        $response = $this->put(route('categories.update', ['category' => 2]), []);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
