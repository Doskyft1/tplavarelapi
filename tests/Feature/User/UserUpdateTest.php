<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserUpdateTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testUpdate()
    {
        $faker = Factory::create();

        $response = $this->put(route('users.update', ['user' => 2]), [
            'name' => $faker->name,
            'email' => $faker->email
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateNotFound()
    {
        $faker = Factory::create();

        $response = $this->put(route('users.update', ['user' => 1000000000]), [
            'name' => $faker->name,
            'email' => $faker->email
        ]);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateMissNameArgument()
    {
        $faker = Factory::create();

        $response = $this->put(route('users.update', ['user' => 2]), [
            'name' => $faker->name
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
