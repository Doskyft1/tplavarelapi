<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserStoreTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testStore()
    {
        $faker = Factory::create();

        $response = $this->post(route('users.store'), [
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => $faker->password
        ]);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testStoreMissNameArgument()
    {
        $faker = Factory::create();

        $response = $this->post(route('users.store'), [
            'email' => $faker->email,
            'password' => $faker->password
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testStoreBadPasswordArgument()
    {
        $faker = Factory::create();

        $response = $this->post(route('users.store'), [
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => $faker->words // return a array
        ]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
