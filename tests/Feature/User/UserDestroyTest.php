<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserDestroyTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testDestroy()
    {
        $response = $this->delete(route('users.destroy', ['user' => 2]));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDestroyFailed()
    {
        $response = $this->delete(route('users.destroy', ['user' => 1000000]));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
