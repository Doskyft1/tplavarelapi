<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * @beforeClass
     */
    public static function resetDatabase()
    {
        exec('php artisan db:wipe --env=testing');
        exec('php artisan migrate:refresh --env=testing');
        exec('php artisan db:seed --env=testing');
    }

    public function testIndex()
    {
        $response = $this->get(route('users.index'));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShow()
    {
        $response = $this->get(route('users.show', ['user' => 2]));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFailed()
    {
        $response = $this->get(route('users.show', ['user' => 10000000]));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
