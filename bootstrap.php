<?php

require_once __DIR__ . '/vendor/autoload.php';

exec('php artisan db:wipe --env=testing');
exec('php artisan migrate:refresh --env=testing');
exec('php artisan db:seed --env=testing');
