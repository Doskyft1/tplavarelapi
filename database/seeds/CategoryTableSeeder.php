<?php

use App\Category;
use App\Product;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 2)->create()->each(function(Category $c) {
            $c->products()->save(factory(Product::class)->make());
        });
    }
}
