<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'stock', 'price',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public static function rules($id = 0, $merge = [])
    {
        return array_merge([
            'name' => 'required|string',
            'stock' => 'required|integer|max:4294967295|min:0',
            'price' => 'required|numeric|max:999999.99|min:0'
        ], $merge);
    }
}
