<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NotAcceptable
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        if (!array_intersect($request->getAcceptableContentTypes(), ['*/*', 'application/json'])) {
            return new Response('https://www.youtube.com/embed/07So_lJQyqw', Response::HTTP_NOT_ACCEPTABLE);
        }
        return $next($request);
    }
}
