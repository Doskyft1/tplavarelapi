<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UnsupportedMediaType
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getContent() !== null and !json_decode($request->getContent())) {
            return new Response(null, Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }
        if (!in_array($request->getContentType(), [null, 'json'])) {
            return new Response(null, Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }
        return $next($request);
    }
}
