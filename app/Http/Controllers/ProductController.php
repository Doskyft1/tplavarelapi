<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse(Product::all(), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate(Product::rules());
        } catch (ValidationException $exception) {
            return new JsonResponse($exception->errors(), Response::HTTP_BAD_REQUEST);
        }

        $properties = array_diff_key($request->all(), [ 'categories' ]);
        $product = Product::create($properties);
        if ($categories = $request->get('categories')) {
            foreach($categories as $category_id) {
                try {
                    Category::findOrFail($category_id)->products()->save($product);
                } catch (Exception $exception) {
                    return new JsonResponse("Category #$category_id not found", Response::HTTP_NOT_FOUND);
                }
            }
        }


        return new JsonResponse($product, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product)
    {
        return new JsonResponse($product, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return JsonResponse
     */
    public function update(Request $request, Product $product)
    {
        try {
            $request->validate(Product::rules(0));
        } catch (ValidationException $exception) {
            return new JsonResponse($exception->errors(), Response::HTTP_BAD_REQUEST);
        }

        $properties = array_diff_key($request->all(), [ 'categories' ]);
        $product->update($properties);
        if ($categories = $request->get('categories')) {
            foreach ($categories as $category_id) {
                try {
                    Category::findOrFail($category_id)->products()->save($product);
                } catch (Exception $exception) {
                    return new JsonResponse("Category #$category_id not found", Response::HTTP_NOT_FOUND);
                }
            }
        }

        return new JsonResponse($product, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();
        } catch (Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return JsonResponse
     */
    public function restock(Request $request, Product $product)
    {
        $product->stock = $request->get('stock');
        $product->save();

        return new JsonResponse($product, Response::HTTP_OK);
    }
}
