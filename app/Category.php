<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public static function rules($id = 0, $merge = [])
    {
        return array_merge([
            'name' => 'required'
        ], $merge);
    }
}
